package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testPlanet() throws MarsRoverException{
	
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacles);
		
		boolean obstacle = rover.planetContainsObstacleAt(4, 7);
		
		assertTrue(obstacle);
		
	}

}
